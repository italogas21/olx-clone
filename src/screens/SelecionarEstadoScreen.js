import React, { Component } from 'react';
// eslint-disable-next-line max-len
import { Container, Content, List, ListItem, Text } from 'native-base';

export default class SelecionarEstadoScreen extends Component {
  render() {
    return (
      <Container>
        <Content>
          <List>
            <ListItem><Text>Distrito Federal</Text></ListItem>
            <ListItem><Text>Goias</Text></ListItem>
            <ListItem><Text>Mato Grosso</Text></ListItem>
            <ListItem><Text>Mato Grosso do Sul</Text></ListItem>
            <ListItem><Text>Alagoas</Text></ListItem>
            <ListItem><Text>Bahia</Text></ListItem>
            <ListItem><Text>Ceara</Text></ListItem>
            <ListItem><Text>Maranhao</Text></ListItem>
            <ListItem><Text>Paraiba</Text></ListItem>
            <ListItem><Text>Pernambuco</Text></ListItem>
            <ListItem><Text>Piaui</Text></ListItem>
            <ListItem><Text>Rio Grande do Norte</Text></ListItem>
            <ListItem><Text>Sergipe</Text></ListItem>
            <ListItem><Text>Acre</Text></ListItem>
            <ListItem><Text>Amapa</Text></ListItem>
            <ListItem><Text>Amazonas</Text></ListItem>
            <ListItem><Text>Para</Text></ListItem>
            <ListItem><Text>Rondonia</Text></ListItem>
            <ListItem><Text>Roraima</Text></ListItem>
            <ListItem><Text>Tocantins</Text></ListItem>
            <ListItem><Text>Espirito Santo</Text></ListItem>
            <ListItem><Text>Minas Gerais</Text></ListItem>
            <ListItem><Text>Rio de Janeiro</Text></ListItem>
            <ListItem><Text>Sao Paulo</Text></ListItem>
            <ListItem><Text>Parana</Text></ListItem>
            <ListItem><Text>Rio Grande do Sul</Text></ListItem>
            <ListItem><Text>Santa Catarina</Text></ListItem>
          </List>
        </Content>
      </Container>
    );
  }
}
