import React, { Component } from 'react';
import { Text, View, TextInput, Button, TouchableHighlight } from 'react-native';
import { Actions } from 'react-native-router-flux';

export default class FormLogin extends Component {
  render() {
      return (
        <View style={{ flex: 1, padding: 10 }}>
          <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
              <Text style={{ fontSize: 45 }}> OLX Clone </Text>
          </View>
          <View style={{ flex: 2 }}>
              <TextInput style={{ fontSize: 20, height: 45 }} placeholder='Email' />
              <TextInput style={{ fontSize: 20, height: 45 }} placeholder='Senha' />
              <TouchableHighlight onPress={() => Actions.cadastroScreen()} >
                <Text style={{ fontSize: 20 }}> Nao possui cadastro? Cadastre-se </Text>
              </TouchableHighlight>
          </View>
          <View style={{ flex: 2 }}>
              <Button title='Acessar' onPress={() => Actions.barraTabs()} />
          </View>
        </View>
      );
  }
}
