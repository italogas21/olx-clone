import React, { Component } from 'react';
// eslint-disable-next-line max-len
import { Container, Content, ListItem, Text, Left, Body, List, Thumbnail } from 'native-base';

export default class SelecionarCategoriaScreen extends Component {
  render() {
    return (
      <Container>
        <Content>
          <List>
            <ListItem thumbnail>            
              <Left><Thumbnail square source={{ uri: 'https://img-aws.ehowcdn.com/140x140p/photos.demandstudios.com/184/67/fotolia_4254707_XS.jpg' }} /></Left> 
              <Body><Text>Todas as categorias </Text></Body>
            </ListItem>
            <ListItem thumbnail>            
              <Left><Thumbnail square source={{ uri: 'https://img-aws.ehowcdn.com/140x140p/photos.demandstudios.com/184/67/fotolia_4254707_XS.jpg' }} /></Left> 
              <Body><Text>Todas as categorias </Text></Body>
            </ListItem>
            <ListItem thumbnail>            
              <Left><Thumbnail square source={{ uri: 'https://img-aws.ehowcdn.com/140x140p/photos.demandstudios.com/184/67/fotolia_4254707_XS.jpg' }} /></Left> 
              <Body><Text>Todas as categorias </Text></Body>
            </ListItem>
          </List>
        </Content>
      </Container>
    );
  }
}
