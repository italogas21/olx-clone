import React, { Component } from 'react';
import { Text, View, TextInput, Button } from 'react-native';

export default class CadastroScreen extends Component {
  render() {
      return (
        <View style={{ flex: 1, padding: 10 }}>
          <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
              <Text style={{ fontSize: 45 }}> OLX Clone </Text>
          </View>
          <View style={{ flex: 3 }}>
              <TextInput style={{ fontSize: 20, height: 45 }} placeholder='Nome' />
              <TextInput style={{ fontSize: 20, height: 45 }} placeholder='Email' />
              <TextInput style={{ fontSize: 20, height: 45 }} placeholder='Senha' />
              <TextInput style={{ fontSize: 20, height: 45 }} placeholder='Repetir senha' />
          </View>
          <View style={{ flex: 1 }}>
              <Button title='Cadastrar' onPress={() => false} />
          </View>
        </View>
      );
  }
}
