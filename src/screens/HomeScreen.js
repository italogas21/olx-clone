/* eslint-disable max-len */
import React, { Component } from 'react';
import { Image } from 'react-native';
import { Container, Header, Left, Body, Right, Text, Card, CardItem, Content, Button, View, Icon } from 'native-base';
import { Actions } from 'react-native-router-flux';

export default class HomeScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      active: false
    };
  }
  render() {
    return (
    <Container>
      <Header transparent> 
        <Left>
          <Button>
            <Icon android="md-menu" />
          </Button>
        </Left>
        <Right>
          <Button>
            <Icon android="md-menu" />
          </Button>
        </Right>
      </Header>
      <Content>
        <View style={{ flexDirection: 'row', elevation: 5 }}>
          <Button transparent style={{ flex: 1, justifyContent: 'center', borderWidth: 1 }} onPress={() => Actions.selecionarEstadoScreen()}><Text>Estado</Text></Button>
          <Button transparent style={{ flex: 1, justifyContent: 'center', borderWidth: 1 }} onPress={() => Actions.selecionarCategoriaScreen()}><Text>Categoria</Text></Button>
          <Button transparent style={{ flex: 1, justifyContent: 'center', borderWidth: 1 }} onPress={() => Actions.filtrosScreen()}><Text>FIltros</Text></Button>
        </View>
        <View>
          <Card>
            <CardItem cardBody>
              <Left style={{ flex: 1 }}>
                <Image source={{ uri: 'https://img-aws.ehowcdn.com/140x140p/photos.demandstudios.com/184/67/fotolia_4254707_XS.jpg' }} style={{ height: 100, width: 100 }} />
              </Left>
              <Body style={{ flex: 2 }}>
                  <Text style={{ paddingTop: 10, fontSize: 12, fontWeight: '200' }}>Descricao do produto</Text>
                  <Text style={{ paddingTop: 10, fontSize: 14, fontWeight: 'bold' }}>R$ 20,00</Text>
                  <Text style={{ paddingTop: 20, fontSize: 10, fontWeight: '100' }}>12 de maio, Centenario</Text>
              </Body>
            </CardItem>
          </Card>
          <Card>
            <CardItem cardBody>
              <Left style={{ flex: 1 }}>
                <Image source={{ uri: 'https://img-aws.ehowcdn.com/140x140p/photos.demandstudios.com/184/67/fotolia_4254707_XS.jpg' }} style={{ height: 100, width: 100 }} />
              </Left>
              <Body style={{ flex: 2 }}>
                  <Text style={{ paddingTop: 10, fontSize: 12, fontWeight: '200' }}>Descricao do produto</Text>
                  <Text style={{ paddingTop: 10, fontSize: 14, fontWeight: 'bold' }}>R$ 20,00</Text>
                  <Text style={{ paddingTop: 20, fontSize: 10, fontWeight: '100' }}>12 de maio, Centenario</Text>
              </Body>
            </CardItem>
          </Card>
        </View>
        <View>
          <Button full warning style={{ flexDirection: 'column', alignContent: 'flex-end' }}>
              <Text>Anunciar agora</Text>
          </Button>
        </View>
      </Content>
    </Container>
    );
  }
}

