/* eslint-disable max-len */
import React from 'react';
import { Router, Scene } from 'react-native-router-flux';

import LoginScreen from './screens/LoginScreen';
import CadastroScreen from './screens/CadastroScreen';
import HomeScreen from './screens/HomeScreen';
import SelecionarEstadoScreen from './screens/SelecionarEstadoScreen';
import SelecionarCategoriaScreen from './screens/SelecionarCategoriaScreen';
import FiltrosScreen from './screens/FiltrosScreen';


function Routes() {
    return (
        <Router>
            <Scene key='root'>
                <Scene key='homeScreen' component={HomeScreen} navTransparent />
                <Scene key='loginScreen' component={LoginScreen} title="Login" />
                <Scene key='cadastroScreen' component={CadastroScreen} title="Cadastro" />
                <Scene key='selecionarEstadoScreen' component={SelecionarEstadoScreen} title="Selecionar Estado" />
                <Scene key='selecionarCategoriaScreen' component={SelecionarCategoriaScreen} title="Selecionar Categoria" />
                <Scene key='filtrosScreen' component={FiltrosScreen} title="Filtros" />
            </Scene>            
        </Router>
    );
}

export default Routes;
